package db_connect;

import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
            try {
                Connection newConnection = dbConnector.createConnect();
                String query = "INSERT INTO student (`name`, `age`, `gender`, `phone`) VALUES (?, ?, ?, ?)";
                PreparedStatement newStatement = newConnection.prepareStatement(query);
                newStatement.setString(1, student.getName());
                newStatement.setInt(2, student.getAge());
                newStatement.setString(3, student.getGender());
                newStatement.setString(4, student.getPhone());
                newStatement.execute();
                newConnection.close();
                return true;
            } catch (SQLException ignored) {
            }

        return false;
    }
    
    public List<Student> findStudents() {
        List<Student> studentList = new ArrayList<>();
        try {
            Connection newConnection = dbConnector.createConnect();
            String query = "SELECT * FROM student";
            PreparedStatement newStatement = newConnection.prepareStatement(query);
            ResultSet result = newStatement.executeQuery();
            while (result.next()) {
                studentList.add(new Student(result.getInt("id"), result.getString("name"), result.getInt("age"),
                        result.getString("gender"), result.getString("phone")));
            }
            newConnection.close();
        } catch (SQLException ignored) {
        }
        return studentList;
    }
    
    public boolean updateStudent(Student student) {
        try {
            Connection newConnection = dbConnector.createConnect();
            String query = "UPDATE student SET `name` = ?, `age` = ?, `gender` = ?, `phone` = ? WHERE `id` = ?";
            PreparedStatement newStatement = newConnection.prepareStatement(query);
            newStatement.setString(1, student.getName());
            newStatement.setInt(2, student.getAge());
            newStatement.setString(3, student.getGender());
            newStatement.setString(4, student.getPhone());
            newStatement.setInt(5, student.getId());
            newStatement.execute();
            newConnection.close();
            return true;
        } catch (SQLException ignored) {
        }
        return false;
    }
    
    public boolean deleteStudent(Student student) {
        try {
            Connection newConnection = dbConnector.createConnect();
            String query = "DELETE FROM student WHERE `id` = ?";
            PreparedStatement newStatement = newConnection.prepareStatement(query);
            newStatement.setInt(1, student.getId());
            newStatement.execute();
            newConnection.close();
            return true;
        } catch (SQLException ignored) {
        }
        return false;
    }
}
