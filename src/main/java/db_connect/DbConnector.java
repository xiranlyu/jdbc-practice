package db_connect;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {
    
    private DbConfig dbConfig;
    
    public DbConnector() {
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("application.yml");
        this.dbConfig = yaml.load(inputStream);
    }
    
    public DbConfig getDbConfig() {
        return dbConfig;
    }
    
    public Connection createConnect() throws SQLException {
            try {
                //加载驱动
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Cannot find the driver!");
            }
            Connection connection = null;
            try {
                //创建连接
                connection = DriverManager.getConnection(dbConfig.getDatabaseURL(), dbConfig.getUsername(), dbConfig.getPassword());
            } catch (SQLException e) {
                System.out.println("Failed to connect!");
            }
            return connection;
    }
}
